package blackboxtest

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"testing"
)

var verbose = true
var dir = "/tmp/test"

func cleanDir(directory string) error {
	_, err := exec.Command("sh", "-c", "rm -rf /tmp/test").Output()

	//if err != nil {
	//	return err
	//}

	err = os.Mkdir(directory, os.ModePerm)

	if err != nil {
		return err
	}

	return nil
}

func execFunc(command string, directory string) ([]byte, *error) {
	cmd := exec.Command("sh", "-c", command)
	cmd.Dir = directory
	out, err := cmd.Output()

	if verbose {
		if err != nil {
			fmt.Println(err.Error())
		}
		if out != nil {
			fmt.Println(string(out))
		}
	}

	return out, &err
}

func execMultipleFun(testParams [][]string) error {
	for i := range testParams {
		out, err := execFunc(testParams[i][0], testParams[i][1])

		if testParams[i][2] == "check" {
			if *err != nil {
				fmt.Println("Error running " + testParams[i][0])
				return *err
			}
		} else if testParams[i][2] != "ignore" {
			if out == nil {
				return errors.New("No output to check")
			}
			if !strings.Contains(string(out), testParams[i][2]) {
				return errors.New(string(out) + " does not contain " + testParams[i][2])
			}
		}
	}

	return nil
}

func TestFDKCli(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping create test")
	}

	cleanDir(dir)

	params := [][]string{
		{"fn init --runtime go hello", dir, "check"},
		{"fn delete app myapp", dir + "/hello", "ignore"},
		{"fn create app myapp", dir + "/hello", "check"},
		{"fn --verbose deploy --app myapp --local", dir + "/hello", "check"},
		{"fn invoke myapp hello", dir + "/hello", "Hello"},
	}

	err := execMultipleFun(params)

	if err != nil {
		t.Error("Problem with running " + err.Error())
	}
}

func TestPlayground(t *testing.T) {
	type APIResponseItem struct {
		id int `json:"id"`
	}
	fmt.Println("here 2")
	type APIResponse struct {
		Item []*APIResponseItem `json:"items"`
	}

	appId := ""

	response, err := http.Get("http://localhost:8080/v2/apps?name=myapp")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(data))

		obj := map[string]interface{}{}
		if err := json.Unmarshal(data, &obj); err != nil {
			log.Fatal(err)
		}

		n := obj["items"].([]interface{})
		appId = n[0].(map[string]interface{})["id"].(string)
		fmt.Println("App id is " + appId)
	}

	fnName := "hello"
	invocationID := ""

	response, err = http.Get("http://localhost:8080/v2/fns?app_id=" + appId + "&name=" + fnName)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(data))

		obj := map[string]interface{}{}
		if err := json.Unmarshal(data, &obj); err != nil {
			log.Fatal(err)
		}

		n := obj["items"].([]interface{})
		invocationID = n[0].(map[string]interface{})["id"].(string)
		fmt.Println("Invoke id is " + invocationID)
	}

	buf := new(bytes.Buffer)
	response, err = http.Post("http://localhost:8080/invoke/"+invocationID, "json", buf)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(data))
	}
}
