package blackboxtest

import (
	"fmt"
	"os"
	"testing"
)

func TestPumba(t *testing.T) {
	pumbaExec := "pumba"

	//fmt.Println(os.Getenv("PUMBA_EXEC"))
	if value, ok := os.LookupEnv("PUMBA_EXEC"); ok {
		pumbaExec = value
	}

	if testing.Short() {
		t.Skip("skipping create test")
	}

	cleanDir(dir)
	fmt.Println(dir)

	params := [][]string{
		{"fn init --runtime go hello", dir, "check"},
		{"sed -ie '/\\\"io\\\"/a \\\\t\\\"time\\\"' func.go", dir + "/hello", "ignore"},
		{"sed -ie '/Decode(p)/a \\\\ttime.Sleep(20 * time.Second)' func.go", dir + "/hello", "ignore"},
		{"fn delete app myapp", dir + "/hello", "ignore"},
		{"fn create app myapp", dir + "/hello", "check"},
	}

	err := execMultipleFun(params)

	if err != nil {
		t.Error("Problem with running " + err.Error())
	}

	params2 := [][]string{
		{"fn --verbose deploy --app myapp --local", dir + "/hello", "check"},
		{"fn invoke myapp hello", dir + "/hello", "Hello"},
	}

	for i := 0; i < 5; i++ {
		go func() {
			err = execMultipleFun(params2)

			if err != nil {
				t.Error("Problem with running " + err.Error())
			}
		}()
	}

	//Run Pumba and kill a random container with SIGKILL
	go func() {
		_, err := execFunc(pumbaExec+" --random --interval 20s kill --signal SIGKILL", ".")

		if *err != nil {
			t.Error("Problem running pumba " + (*err).Error())
		}
	}()

	err = execMultipleFun(params2)

	if err != nil {
		t.Error("Problem with running " + err.Error())
	}
}
