package server

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
)

type CronJob struct {
	//c         *gin.Context
	fnID      string
	sleepTime int
}

var cronJob CronJob

func (s *Server) cronRun() {
	type APIResponseItem struct {
		id int `json:"id"`
	}

	type APIResponse struct {
		Item []*APIResponseItem `json:"items"`
	}

	logrus.Info("Cron job daemon started ")

	cronTime := 20
	for {
		if cronJob.sleepTime > 0 {
			cronTime = cronJob.sleepTime
		}
		time.Sleep(time.Duration(cronTime) * time.Second)
		//logrus.Info("Waking up for Cron")
		if cronJob.fnID == "" {
			//logrus.Info("No cronjob")
		} else {
			//logrus.Info("Cron job found with a sleep of " + strconv.Itoa(cronJob.sleepTime))
			//fmt.Printf("%v\n", cronJob.fnID)

			buf := new(bytes.Buffer)
			response, err := http.Post("http://localhost:8080/invoke/"+cronJob.fnID+"/"+strconv.Itoa(cronJob.sleepTime), "json", buf)
			if err != nil {
				fmt.Printf("The HTTP request failed with error %s\n", err)
			} else {
				data, _ := ioutil.ReadAll(response.Body)
				fmt.Println("Running a cron job, results are as follows: ")
				fmt.Println(string(data))
			}

			//s.handleFnInvokeCall(cronJob.c)
		}

	}
}
